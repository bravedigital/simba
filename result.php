<?php
include_once("functions.php");

if ($_SESSION['gotopage'] != "result.php") {
    $host = $_SERVER['HTTP_HOST'];
    $url = "https://" . $host  . "/" . $_SESSION['gotopage'];
    header("Location: " . $url);
}

$won = $_SESSION['won'];

if (isset($_GET['won'])) {
    $won = true;
}
?>
<!DOCTYPE HTML>
<html>

<head>
    <title>Simba Sounds Of Flavour! Result</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/x-icon" href="img/favicon.png" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/styles.css?c=<?=time()?>">
<?php
include_once("analytics.php");
?>
</head>

<body>
    <div class="containermain container-fluid">
<?php
include_once("rowheader.php");

if (!$won) {
?>
        <div class="row">
            <div class="col-12 text-center">
                <img src="assets/roars.svg" class="imgMainCampaingTitle">
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <span class="spnLose1">BETTER LUCK NEXT TIME!</span>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-12 text-center">
                <span class="spnLose2">Purchase another new Simba flavour pack to try again.</span>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-12 text-center">
                <img src="assets/scanhr.png" class="imgCell">
            </div>
        </div>
        <div class="row my-4">
            <div class="col-12 text-center">
                <span class="spnLose3">Show your pack to one of our Simba brand ambassadors and scan their code to try again!</span>
            </div>
        </div>
<?php
}
else {
?>
        <form id="frmDetails2" action="savedetails2.php" method="post">
            <div class="row">
                <div class="col-12 text-center">
                    <img src="assets/winner.png">
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <span class="spnInfo">In order to send you your store voucher ... we need the following information:</span>
                </div>
            </div>
            <div class="row my-4">
                <div class="col-2">
                    <span class="spnRoundNumber">01</span>
                </div>
                <div class="col-10">
                    <span class="spnDetailsText spnDetailsText2">YOUR FULL NAME</span>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12">
                    <input type="text" name="txtName" id="txtName" class="txtEntry">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <img src="assets/hr.svg" class="imgHR">
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-2">
                    <span class="spnRoundNumber">02</span>
                </div>
                <div class="col-10">
                    <span class="spnDetailsText spnDetailsText2">CELL NUMBER</span>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12">
                    <input type="text" name="txtCell" id="txtCell" class="txtEntry">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <img src="assets/hr.svg" class="imgHR">
                </div>
            </div>
            <div class="row my-4">
                <div class="col-12">
                    <div id="divButtons">
                        <button type="submit" class="btn btn-success btn-lg btnRedeem" id="btnSpin">REDEEM MY PRIZE!</button>
                    </div>
                </div>
            </div>
        </form>
<?php
}

include_once("rowfooter.php");
?>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<script>
$(function () {
    $("#btnSpin").on("click", function() {
        var name = $('#txtName').val();
        var cell = $('#txtCell').val();
        if (name.length == 0 || cell.length == 0) {
            return false;
        }
    });
});
</script>

</body>

</html>