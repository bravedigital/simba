<?php
include_once("functions.php");

if (isset($_GET['simba'])) {
    $token = getRandomHex(8);
    $secrettoken = getRandomHex(8);

    $_SESSION['token'] = $token;
    $_SESSION['secrettoken'] = $secrettoken;
    $_SESSION['won'] = false;
    $_SESSION['winningcode'] = "";
    $_SESSION['winningtoken'] = "";
    $_SESSION['winnername'] = "Anonymous";
    $_SESSION['gotopage'] = "details.php";

    $sql = "INSERT INTO sessions (token, shopid) VALUES ('" . $token . "', 1)";
    $result = mysqli_query($conn, $sql);

    $host = $_SERVER['HTTP_HOST'];
    $url = "https://" . $host . "/details.php?token=" . $token;
    header("Location: " . $url);
}
else {
    echo "Not allowed";
    die;
}
?>