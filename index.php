<!DOCTYPE HTML>
<html>

<head>
    <title>Simba Sounds Of Flavour! Home</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/x-icon" href="img/favicon.png" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/styles.css?c=<?=time()?>">
<?php
include_once("analytics.php");
?>
</head>

<body>
    <div class="containermain">
<?php
include_once("rowheader.php");
?>
        <div class="row">
            <div class="col-md-12 text-center">
                <img src="assets/roars.svg" class="imgMainCampaingTitle">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center divSubline">
                <object data="assets/subline2.svg"></object>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-12 text-center">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="true">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <img src="assets/Simba_export_120g_render_mex.png" class="d-block w-50">
                        </div>
                        <div class="carousel-item">
                            <img src="assets/Simba_export_120g_render_tomato.png" class="d-block w-50">
                        </div>
                        <div class="carousel-item">
                            <img src="assets/Simba_export_120g_render_chutney.png" class="d-block w-50">
                        </div>
                        <div class="carousel-item">
                            <img src="assets/Simba_export_120g_render_beef.png" class="d-block w-50">
                        </div>
                        <div class="carousel-item">
                            <img src="assets/Creamy-cheddar-120g-Simba-Export.png" class="d-block w-50">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="row mt-5 extramargin">
            <div class="col-md-12 text-center">
                <a href="howtoplay.php" class="btnHowToPlay">HOW TO PLAY</a>
            </div>
        </div>
<?php
include_once("rowfooter.php");
?>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
$(function () {
    $('#carouselExampleIndicators').carousel({
        interval: 10000
    });
});
    </script>
</body>

</html>