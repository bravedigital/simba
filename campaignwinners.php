<?php
if (isset($_GET['accesscode'])) {
    $accesscode = $_GET['accesscode'];
    if ($accesscode != "b8bf13ae300c3cb5") {
        die;
    }

    include_once("functions.php");

    $tabledata = "";

    $countsql = "SELECT name,
                cell,
                store,
                voucher,
                dateadded
                FROM entries
                WHERE name > ''
                ORDER BY name ASC";
    $result = mysqli_query($conn, $countsql);
    $resultcount = mysqli_num_rows($result);
    if ($resultcount > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $name = $row['name'];
            $cell = $row['cell'];
            $store = $row['store'];
            $voucher = $row['voucher'];
            $dateadded = $row['dateadded'];

            $tabledata .= "<tr>";
            $tabledata .= "<td>" . $name . "</td>";
            $tabledata .= "<td>" . $cell . "</td>";
            $tabledata .= "<td>" . $store . "</td>";
            $tabledata .= "<td>" . $voucher . "</td>";
            $tabledata .= "<td>" . $dateadded . "</td>";
            $tabledata .= "</tr>";
        }
    }
    else {
        die;
    }
}
else {
    die;
}
?>
<!DOCTYPE HTML>
<html>

<head>
    <title>Simba Sounds Of Flavour! Campaign Winners</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/x-icon" href="img/favicon.png" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.0/css/jquery.dataTables.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/styles.css?c=<?=time()?>">
<?php
include_once("analytics.php");
?>
</head>

<body>
    <div class="containermain">
<?php
include_once("rowheader.php");
?>
        <div class="row mt-5">
            <div class="col-md-12"><h2>Simba Sounds of Flavour!</h2></div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">
                <table id="tblCampaignData">
                    <thead>
                        <th>Name</th>
                        <th>Cell</th>
                        <th>Store</th>
                        <th>Voucher</th>
                        <th>Date Won</th>
                    </thead>
                    <tbody>
<?php
echo $tabledata;
?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.js"></script>

<script type="text/javascript">
$(function () {
    $("#tblCampaignData").DataTable({
        dom: "frti",
        pageLength: -1,
        "oLanguage": {
            "sSearch": "Filter:"
        },
        "aaSorting": [ [0, 'asc'] ]
    });
});
</script>
</body>
</html>