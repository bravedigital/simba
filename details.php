<?php
include_once("functions.php");

if ($_SESSION['gotopage'] != "details.php") {
    $host = $_SERVER['HTTP_HOST'];
    $url = "https://" . $host  . "/" . $_SESSION['gotopage'];
    header("Location: " . $url);
}
?>
<!DOCTYPE HTML>
<html>

<head>
    <title>Simba Sounds Of Flavour! Details</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/x-icon" href="img/favicon.png" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/styles.css?c=<?=time()?>">
<?php
include_once("analytics.php");
?>
</head>

<body>
    <div class="containermain container-fluid">
<?php
include_once("rowheader.php");
?>
        <form id="frmDetails" action="savedetails.php" method="post">
            <div class="row">
                <div class="col-12 text-center">
                    <img src="assets/roars.svg" class="imgMainCampaingTitle">
                </div>
            </div>
            <div class="row my-4">
                <div class="col-2">
                    <span class="spnRoundNumber">01</span>
                </div>
                <div class="col-10">
                    <span class="spnDetailsText">ENTER THE LAST 4 BARCODE DIGITS ON THE BACK OF YOUR SIMBA PACK</span>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-4">
                    <img src="assets/barcode.png" class="imgBarcode">
                </div>
                <div class="col-8">
                    <input type="text" name="txtBarcode" id="txtBarcode" class="txtEntry" maxlength="4" required>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <img src="assets/hr.svg" class="imgHR">
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-2">
                    <span class="spnRoundNumber">02</span>
                </div>
                <div class="col-10">
                <span class="spnDetailsText spnDetailsText2">SELECT THE PARTICIPATING STORE</span>
                </div>
            </div>
            <div class="row my-4">
                <div class="col-12">
                    <select name="selStore" id="selStore">
                        <option value="">Please Select</option>
                        <optgroup label="Botswana">
                            <option value="Botswana Choppies">Choppies</option>
                            <option value="Botswana Choppies Westgate">Choppies Westgate</option>
                            <option value="Botswana New Fours">New Fours</option>
                            <option value="Botswana Pick n Pay">Pick n Pay</option>
                            <option value="Botswana Sefalana">Sefalana</option>
                            <option value="Botswana Sefalana Hyper Gabz">Sefalana Hyper Gabz</option>
                            <option value="Botswana Shopper Setlhoa">Shopper Setlhoa</option>
                            <option value="Botswana Shoppers Molapo">Shoppers Molapo</option>
                            <option value="Botswana Spar">Spar</option>
                            <option value="Botswana Spar Railpark">Spar Railpark</option>
                            <option value="Botswana Square Mart">Square Mart</option>
                            <option value="Botswana Trans Africa">Trans Africa</option>
                            <option value="Botswana Trans Hyper">Trans Hyper</option>
                        </optgroup>
                        <optgroup label="Namibia">
                            <option value="BEUKES SPAR">BEUKES SPAR</option>
                            <option value="Namibia CHECKERS MAERUA MALL">CHECKERS MAERUA MALL</option>
                            <option value="Namibia CHECKERS THE GROVE">CHECKERS THE GROVE</option>
                            <option value="Namibia CHECKERS WERNHILL PARK">CHECKERS WERNHILL PARK</option>
                            <option value="Namibia CHOPPIES SUPERSTORE KHOMAS">CHOPPIES SUPERSTORE KHOMAS</option>
                            <option value="Namibia CHECKERS SWAKOPMUND">CHECKERS SWAKOPMUND</option>
                            <option value="Namibia CHECKERS THE DUNES">CHECKERS THE DUNES</option>
                            <option value="Namibia KUISEB SHOP 4 VALUE">KUISEB SHOP 4 VALUE</option>
                            <option value="Namibia MAERUA SUPERSPAR">MAERUA SUPERSPAR</option>
                            <option value="Namibia MEGASAVE HEFER SWAKOPMUND">MEGASAVE HEFER SWAKOPMUND</option>
                            <option value="Namibia MEGASAVE NAMICA">MEGASAVE NAMICA</option>
                            <option value="Namibia METRO HYPER">METRO HYPER</option>
                            <option value="Namibia METRO MEGA STORE SWAKOPMUND">METRO MEGA STORE SWAKOPMUND</option>
                            <option value="Namibia METRO NORTHERN INDUSTRIAL WINDHOEK">METRO NORTHERN INDUSTRIAL WINDHOEK</option>
                            <option value="Namibia METRO SWAKOPMUND">METRO SWAKOPMUND</option>
                            <option value="Namibia METRO WALVIS BAY">METRO WALVIS BAY</option>
                            <option value="Namibia MODEL PICK 'N PAY AUAS VALLEY">MODEL PICK 'N PAY AUAS VALLEY</option>
                            <option value="Namibia MODEL PICK 'N PAY B1 CITY">MODEL PICK 'N PAY B1 CITY</option>
                            <option value="Namibia MODEL PICK 'N PAY KATUTURA">MODEL PICK 'N PAY KATUTURA</option>
                            <option value="Namibia MODEL PICK 'N PAY MEGA CENTRE">MODEL PICK 'N PAY MEGA CENTRE</option>
                            <option value="Namibia MODEL PICK 'N PAY SWAKOPMUND">MODEL PICK 'N PAY SWAKOPMUND</option>
                            <option value="Namibia MODEL PICK 'N PAY WERNHILL">MODEL PICK 'N PAY WERNHILL</option>
                            <option value="Namibia OCEAN VIEW SPAR">OCEAN VIEW SPAR</option>
                            <option value="Namibia OK FOODS INDEPENDENCE AVE">OK FOODS INDEPENDENCE AVE</option>
                            <option value="Namibia OK FOODS PORTUGUESE MKT">OK FOODS PORTUGUESE MKT</option>
                            <option value="Namibia SENTRA BAINES CENTRE">SENTRA BAINES CENTRE</option>
                            <option value="Namibia SHOPRITE KATUTURA">SHOPRITE KATUTURA</option>
                            <option value="Namibia SHOPRITE LAFRENZ">SHOPRITE LAFRENZ</option>
                            <option value="Namibia SHOPRITE SWAKOPMUND">SHOPRITE SWAKOPMUND</option>
                            <option value="Namibia SHOPRITE WALVIS BAY">SHOPRITE WALVIS BAY</option>
                            <option value="Namibia SHOPRITE WINDHOEK">SHOPRITE WINDHOEK</option>
                            <option value="Namibia SWAKOPMUND SUPERSPAR">SWAKOPMUND SUPERSPAR</option>
                            <option value="Namibia WALVIS BAY SELF SERVICE">WALVIS BAY SELF SERVICE</option>
                            <option value="Namibia WALVIS BAY SUPERSPAR">WALVIS BAY SUPERSPAR</option>
                            <option value="Namibia WB SWAKOPMUND C & C LIQUOR">WB SWAKOPMUND C & C LIQUOR</option>
                            <option value="Namibia WESTLANE SPAR">WESTLANE SPAR</option>
                            <option value="Namibia WINDHOEK CASH & CARRY (PTY) LT">WINDHOEK CASH & CARRY (PTY) LT</option>
                            <option value="Namibia WOERMANN BROCK KHOMAS GOVE">WOERMANN BROCK KHOMAS GOVE</option>
                            <option value="Namibia WOERMANN BROCK KUISEBMOND">WOERMANN BROCK KUISEBMOND</option>
                            <option value="Namibia WOERMANN BROCK MONDESA">WOERMANN BROCK MONDESA</option>
                            <option value="Namibia WOERMANN BROCK NARRAVILLE">WOERMANN BROCK NARRAVILLE</option>
                            <option value="Namibia WOERMANN BROCK OMBILI">WOERMANN BROCK OMBILI</option>
                            <option value="Namibia WOERMANN BROCK OTJOMUISE">WOERMANN BROCK OTJOMUIS</option>
                            <option value="Namibia WOERMANN BROCK VINETA">WOERMANN BROCK VINETA</option>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="row my-4">
                <div class="col-12">
                    <img src="assets/hr.svg" class="imgHR">
                </div>
            </div>
            <div class="row my-4">
                <div class="col-12">
                    <div id="divButtons">
                        <button type="submit" class="btn btn-success btn-lg btnSpinToWin" id="btnSpin">SPIN TO WIN!</button>
                    </div>
                </div>
            </div>
        </form>
<?php
include_once("rowfooter.php");
?>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<script>
$(function () {
    $('#txtBarcode').keypress(function(e) {
        var arr = [];
        var kk = e.which;

        for (i = 48; i < 58; i++)
            arr.push(i);

        if (!(arr.indexOf(kk)>=0))
            e.preventDefault();
    });

    $("#btnSpin").on("click", function() {
        var bc = $('#txtBarcode').val();
        var store = $('#selStore').val();
        if (bc.length != 4 || store == "") {
            return false;
        }
    });
});

window.addEventListener( "pageshow", function ( event ) {
  var historyTraversal = event.persisted || 
                         ( typeof window.performance != "undefined" && 
                              window.performance.navigation.type === 2 );
  if ( historyTraversal ) {
    // Handle page restore.
    window.location.reload();
  }
});
</script>

</body>

</html>