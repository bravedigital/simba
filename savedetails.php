<?php
include_once("functions.php");

$token = $_SESSION['token'];
$secrettoken = $_SESSION['secrettoken'];
// echo $secrettoken;
// die;

if (!ctype_xdigit($token)) {
    echo 2;
    die;
}
if (!ctype_xdigit($secrettoken)) {
    echo 3;
    die;
}

if ($token != "" && isset($_POST['txtBarcode']) && isset($_POST['selStore'])) {
    $barcode = $_POST['txtBarcode'];
    $store = $_POST['selStore'];
    $_SESSION['store'] = $store;

    $barcode = mysqli_real_escape_string($conn, $barcode);
    $barcode = str_replace("%", "\%", $barcode);

    $store = mysqli_real_escape_string($conn, $store);
    $store = str_replace("_", "\_", $store);

    $entriessql = "INSERT INTO entries (token, barcode, store, dateadded) VALUES ('" . $token . "', '" . $barcode . "', '" . $store . "', NOW())";
    mysqli_query($conn, $entriessql);

    $_SESSION['gotopage'] = "play.php";

    $host = $_SERVER['HTTP_HOST'];
    $url = "https://" . $host  . "/" . $_SESSION['gotopage'];
    header("Location: " . $url);
}
else {
    die;
}

?>