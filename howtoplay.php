<!DOCTYPE HTML>
<html>

<head>
    <title>Simba Sounds Of Flavour! How To Play</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/x-icon" href="img/favicon.png" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/styles.css?c=<?=time()?>">
<?php
include_once("analytics.php");
?>
</head>

<body>
    <div class="containermain">
<?php
include_once("rowheader.php");
?>
        <div class="row">
            <div class="col-md-12 text-center">
                <object data="assets/new_howtoplay2.svg"></object>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="divInstructionsPacks"><img src="assets/instructions_packs.png"></div>
                <object data="assets/instructions_updated4.svg"></object>
                <div class="divInstructionsSpinner"><img src="assets/instructions_spinner.png"></div>
            </div>
        </div>
<?php
include_once("rowfooter.php");
?>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
$(function () {
    $('#carouselExampleIndicators').carousel();
});
    </script>
</body>

</html>