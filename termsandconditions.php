<!DOCTYPE HTML>
<html>

<head>
    <title>Simba Sounds Of Flavour! T&C</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/x-icon" href="img/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/styles.css?c=<?=time()?>">
<?php
include_once("analytics.php");
?>
</head>

<body>
    <div class="containermain">
<?php
include_once("rowheader.php");
?>
        <!-- <div class="row">
            <div class="col-md-12 text-center">
                <object data="assets/your_privacy2.svg"></object>
            </div>
        </div> -->
        <div class="row mt-5">
            <div class="col-md-10 divYourPrivacy">
<h6>TERMS AND CONDITIONS</h6>
SIMBA® (Botswana)  Promotional Competition.<br><br>
Full Terms and Conditions to be hosted on the  Simba Microsite page:<br><br>
<a href="//www.simbasoundsofflavour.co.bw">www.simbasoundsofflavour.co.bw</a><br><br>

1. Simba Sounds of flavour is a campaign competition designed to launch the new Simba packaging in Botswana. Simba challenges it’s consumers to reimagine what the sound of flavour means to them. The concept “Sounds of Flavour” ties in with the brands Television Commercial (TVC) created for the brand relaunch where emphasis is placed on the trademark “Roars With Flavour” and combined with the TVC sets the framework for what Simba Botswana Campaign will be.<br><br>
2. Please read the competition terms and conditions (“Terms”) carefully. These Terms apply to all persons entering the Simba Sounds of Flavour promotional competition (“Competition”) conducted by PepsiCo Simba (Proprietary) Limited, situated on the 6th Floor, 144 Oxford Road, Rosebank, Johannesburg, South Africa, 2196, Tel: 011 928 6000 (“Simba”) and Alvi Gordon, situated at Fairway Office Park (Sable Place), 52 Grosvenor Road, Bryanston, Johannesburg, collectively (“Promoters”).<br><br>
3. Instructions on how to enter the Competition, including all rules, directions and prizes all form part of these Terms. If you take part in this Competition, you expressly agree to be bound by these Terms and accept that the Promoters’ decision is final and that no correspondence will be entered into. Prizes are not transferable, substitutable, or exchangeable in any form unless agreed upon at discretion of the promoter.<br><br>
4. The Competition is open to all citizens and legal residents of Botswana (provided that the entrant is also a resident in Botswana at the time of winning and receiving any Prize herein), who at the time of entering the Competition are over the age of 18 (eighteen) years, and are in possession of a valid Botswana Identity Document (or the case of residents, a valid passport and necessary residency permission). Persons that are excluded from entering the Competition include any person who is a director, member, partner, employee or agent of, or consultant (“Associates”) of the Promoters and/or PepsiCo Inc.(including PepsiCo Inc. affiliates), or any other person who directly or indirectly controls, is employed by or is controlled by Promoters, and/or immediate family members of Associates, who are indirectly or directly connected to or employed by any party in the aforementioned capacities or relationships, their advertising agencies, manufacturers, distributors or bottlers of beverages identified by the trademarks owned by or licensed to PepsiCo Inc. (“Disqualified Persons”).<br><br>
5. The Competition will run from the 6th of September 2021 at 00:01 and will end on the 6th of November 2021 at 23:59 (“Competition Duration”). In total, there are 146 (One hundred and forty six) prizes to be won over 8 (eight) weeks.<br><br>
6. RADIO ENTRY MECHANICS<br><br>
The main themes that the competition will cover is sound(music) and flavour. Listeners must express their understanding of sounds of flavour by making a sound with their favourite Simba packet of chips flavour in order to stand a chance of winning weekly prizes, participants must enter as follows:<br><br>
- Entrants must listen to the afternoon drive radio show on Yarona FM which will be the official host of the competition.<br><br>
- Participants are then required to create a 40 (forty) second video recording of themselves with their favourite Simba Potato Chip flavour, making musical sounds that best emulate their chosen Simba potato chip flavour.<br><br>
- Participants may use any objects, musical instruments and sounds in the form of beatboxing to enhance their sound of flavour.<br><br>
- The video clip must not exceed 40 (forty) seconds and must be posted on Facebook or Instagram tagging Yarona FM and using the hashtag #Soundsofflavour.<br><br>
- Participants may also enter by sending their Video clips to Yarona FM’s WhatsApp line.<br><br>
7. RADIO ENTRY CONDITIONS - EACH ENTRY MUST:<br><br>
A. Be an original submission (based on the competition entry criteria), created solely by the entrant, and over which the entrant has all necessary rights, title and interest, including copyright;<br><br>
B. Not infringe upon the intellectual property rights, copyright or defame or invade the publicity or privacy rights, of any third party, living or deceased and/or any brand;<br><br>
C. Not use the brand names, trade names, or trade-marks of any third parties;<br><br>
D. Not contain or make any reference to any branded food or beverage products other than the Simba brand;<br><br>
E. The video is not to exceed 40 (forty) seconds in length if in video format;<br><br>
F. Be suitable for family audiences and for display and publication on national television, and social media channels in the sole determination of the Promoter. Without limitation, Entries shall not contain or describe any content that is or contains: the promotion of unlawful behaviour, political in nature, profanity, nudity, explicit sexuality, harmful, threatening, abusive, racist, harassing, tortious, defamatory, vulgar, obscene, libellous, or is hateful, discriminatory or otherwise offensive or objectionable;<br><br>
G. Have been created specifically for this Competition, and have not been previously published, or entered into, or received any awards in any other Competition or competition of any kind or form whatsoever;<br><br>
H. Be consistent with the Promoter’s corporate image;<br><br>
I. Be consistent with local law or applicable and legitimate self-regulation;<br><br>
J. Entrants must not act in a manner which physically endangers themselves or others nor in a manner which is likely to cause injury or physical harm to themselves or others, and;<br><br>
K. Comply with these Official Competition Rules as published and available on the respective Promoter’s microsite; namely www.simbasoundsofflavour.co.bw<br><br>
8. The Promoter reserves the right to use entries (at its sole discretion) to create further marketing material and share these on any media platforms that it so chooses and the entrants acknowledge that he/she and/or any third parties portrayed in the entry videos will not be entitled to receive any payment, title or rights over the creation, or the idea used. The Promoter reserves the right after the Competition Period, at its sole and absolute discretion, with or without notice, to change the Entry submitted as part of the Prize-winning Entry.<br><br>
9. The Promoters reserve the right at any time, in their sole and absolute discretion, to disqualify any entrant and to remove (or to refuse to post) Entries from the Competition that they find or believe not to comply with the conditions above, or otherwise with these Terms, or that seek to defraud the legitimate purpose of this Competition. Without limiting the generality of these Terms.<br><br>
10. Entries that are determined to be, or suspected to be, in the Promoters’ sole discretion, the subject of any scheme designed to compensate individuals for engaging in activities online that directly increases an entrant’s chance of winning may be disqualified from the Competition and/or any associated prize may be forfeited.<br><br>
11. Entries may also be disqualified, removed from the Competition, and any associated prize forfeit in the event that the individual who submitted and entry has engaged, or is suspected of engaging in, methods designed to affect the outcome of the draw for that Entry – or those of any other entry – in violation of these Terms, and to pursue all other applicable rights and remedies.<br><br>
12. By entering the Competition, entrants agree to release and hold harmless the Promoters and their affiliated companies from any and all claims that any product, commercial, advertising, presentation, web content or other material, in any media, throughout the universe, subsequently produced, presented and/or prepared by or on behalf of the Promoters, infringes with regard to any elements, characters or ideas contained in the entry. For greater certainty, the Promoters will not be responsible should any future product, packaging, advertising, promotion, or other independently developed creative, marketing or advertising materials bear any resemblance to any entry or submission, including a submission that is not declared to be the winning entry.<br><br>
13. RADIO JUDGING CRITERIA<br><br>
13.1 The Simba Sounds of Flavour competition entries will be reviewed by a Judging Panel of the Promoter’s choice using the criteria below as a guide to select winners.<br><br>
- Originality = 30%<br><br>
- Creativity = 20%<br><br>
- Brand fit = 10%<br><br>
- Delivery of Sounds of flavour= 20%<br><br>
- Video quality = 20%<br><br>
13.2 Please note that all points and assessing the validity of each entry will be done at the sole discretion of the Promoter, no conversations or disputes will be entered into.<br><br>
13.3 Please note: Entrants can participate in their own environment setting (e.g. workplace or home).<br><br>
13.4 A valid and successful competition entry for a particular week cannot be repeated for another weeks entry.<br><br>
13.5 Prize draw entry will only be assigned if the judging panel deems that submission accurately depicts the weekly competition entry and that it is not a duplicate entry from a previous week.<br><br>
13.6 Entries of an obscene nature or shown to break the law or encourage dangerous behaviour - at the discretion of the promoter - will be disqualified. Your social media account must also be public (not on private) for the entry to be valid. The entrant's social link must lead to a public profile. Inactive or private accounts will result in forfeiture of the entry.<br><br>
13.7 No weekly entries will be accepted later than 23:59 on Sunday, Tuesday and Thursday.<br><br>
14. RADIO PRIZES<br><br>
There is a total prize pool of 146 (One hundred and forty six) prizes in total that are available under this Competition, that the Promoters will give away after each competition week. The prize pool is comprised of:<br><br>
14.1 A total number of 73 grocery shopping vouchers to the value of P370.00 that can be redeemed at a selected retail outlet.<br><br>
14.2 A total number of 73 airtime vouchers to the value of P370.00<br><br>
15. RADIO WINNER SELECTION<br><br>
Winners are announced on Monday, Wednesday and Friday of each week during the Yarona FM – Simba Sounds of Flavour Show at 10:40am. Possible prize winners will be required to participate in an audit verification process (which includes but is not limited to providing their proof of engagement with the competition in accordance with the entry mechanics), together with a copy of their Identity Document and/or Proof of Residency. Once the required audit verification has been completed, confirmed and verified, the possible winners will be declared as winners and will be contacted by Promoters to facilitate delivery/redemption of their prize to their address of choice within Botswana within 3 (three) weeks or 15 (fifteen) working days from the end of the Promotional Period.<br><br>
If winners are unable to take the delivery at the agreed upon date, time and location, they will be required to collect the prize themselves from the Promoters’ nearest distribution hub. If the Promoters are unable to get hold of the winner from the registration/sign up details used to submit the selected entry into the Competition, to complete the audit verification, they will keep trying for 48 (forty eight) hours before potential winner will be deemed to have forfeited the respective prize and a replacement/ substitute winner is drawn in the same manner as the first;<br><br>
15.1 The Promoters reserve the right, during the promotion, to create highlight reels of videos and/or photographs submitted by entrants during the Competition Duration and to publish these highlight reels weekly on the various Yarona FM social media platforms (including but not limited to Facebook and Instagram). By doing this, the Promoters are in no way endorsing a single entry and these highlight reels do not increase the chances of that challenge or entrant winning.<br><br>
16. All entries must be originally made by the entrant themselves. Bulk entries made from trade, consumer groups or third parties will not be accepted. Incomplete or illegible entries, entries by macros or other automated means (including systems which can be programmed to enter), and entries which do not satisfy the requirements of these terms and conditions in full will be disqualified and will not be counted. If it becomes apparent that an entrant is using a computer(s) to circumvent this condition by, for example, the use of 'script', 'brute force', masking their identity by manipulating IP addresses, using identities other than their own, using multiple email addresses or any other automated means in order to increase that entrant's entries into the promotion in a way that is not consistent with the spirit of the promotion, that entrant's entries will be disqualified and any prize award will be void. Entrants may enter as many Sounds of Flavour videos as they want but only one entry will be selected as a valid entry for the particular weekly prize.<br><br>
17. RETAIL ENTRY MECHANIC<br><br>
17.1 Promoters at retailers encourage consumers to purchase the new pack of Simba chips to stand a chance to scan the QR code.<br><br>
17.2 Consumers scan the QR code with their mobile phones and are led to the microsite.<br><br>
17.3 On the microsite consumers can play the Online Slots Game in this manner:<br><br>
- Consumer enters key details (Name; Surname; ID; email and contact number)<br><br>
- Consumer clicks play and the slot game starts spinning<br><br>
- Consumer clicks stop button to stop the spinning (All this on their mobile phones)<br><br>
- If consumer lands on 3 of the same Simba packs, consumer winners a shopping voucher that gets emailed to them electronically<br><br>
18. RETAIL ENTRY CONDITIONS - EACH ENTRY MUST:<br><br>
A. One packet qualifies for one entry;<br><br>
B. The consumer is required to provide a slip as proof of purchase at the selected retailer;<br><br>
C. You must have made a purchase on the day of instore promotion, purchases from the previous days before the activation will not be accepted;<br><br>
D. The product must be a packet of new pack of Simba to qualify;<br><br>
E. Entrants must not act in a manner which physically endangers themselves or others nor in a manner which is likely to cause injury or physical harm to themselves or others, and;<br><br>
F. Comply with these Official Competition Rules as published and available on the respective Promoter’s microsite; namely www.simbasoundsofflavour.co.bw<br><br>
19. RETAIL PRIZES<br><br>
19.1 There is a total prize pool of 480 (Four hundred and eighty) prizes in total that are available under this Competition, that the Promoters will give away after each competition week.<br><br>
19.2 The prize pool is comprised of a total number of 480grocery shopping vouchers that can be redeemed at a selected retail outlet.<br><br>
20. RETAIL WINNER SELECTION<br><br>
20.1 Winners will be email with the voucher number which they will redeem at a selected retailer.<br><br>
20.2 The Promoter reserves the right to use entries (at its sole discretion) to create further marketing material and share these on any media platforms that it so chooses and the entrants acknowledge that he/she will not be entitled to receive any payment, title or rights over the content used. The entrant may decline or withdraw this use by contacting the Promoters.<br><br>
20.3 The Promoter reserves the right after the Competition Period, at its sole and absolute discretion, with or without notice, to change the Entry submitted as part of the Prize-winning Entry.<br><br>
21. The Promoters reserve the right at any time, in their sole and absolute discretion, to disqualify any entrant and to remove (or to refuse to post) Entries from the Competition that they find or believe not to comply with the conditions above, or otherwise with these Terms, or that seek to defraud the legitimate purpose of this Competition. Without limiting the generality of these Terms.<br><br>
22. Entries that are determined to be, or suspected to be, in the Promoters’ sole discretion, the subject of any scheme designed to compensate individuals for engaging in activities online that directly increases an entrant’s chance of winning may be disqualified from the Competition and/or any associated prize may be forfeited.<br><br>
23. Entries may also be disqualified, removed from the Competition, and any associated prize forfeit in the event that the individual who submitted and entry has engaged, or is suspected of engaging in, methods designed to affect the outcome of the draw for that Entry – or those of any other entry – in violation of these Terms, and to pursue all other applicable rights and remedies.<br><br>
24. By entering the Competition, entrants agree to release and hold harmless the Promoters and their affiliated companies from any and all claims that any product, commercial, advertising, presentation, web content or other material, in any media, throughout the universe, subsequently produced, presented and/or prepared by or on behalf of the Promoters, infringes with regard to any elements, characters or ideas contained in the entry. For greater certainty, the Promoters will not be responsible should any future product, packaging, advertising, promotion, or other independently developed creative, marketing or advertising materials bear any resemblance to any entry or submission, including a submission that is not declared to be the winning entry.<br><br>
25. Any attempt to deliberately damage or interfere with the social media pages or microsites used in connection with the promotion or the information on it, or to otherwise undermine the legitimate operation of the promotional competition may be a violation of criminal and civil laws and should such an attempt be made, whether successful or not, the Promoters reserve the right to seek damages to the fullest extent permitted by law. The Promoters reserve the right (subject to applicable law) to disqualify and/or ban any individual who tampers with or attempts to subvert or interfere with the Competition or entry process or prize retrieval process.<br><br>
26. If for any reason the Competition is not capable of running as planned as a result of any technical failures, unauthorized intervention, computer virus, mobile network failure, social media site downtime, tampering, fraud or any other causes beyond the Promoters’ control which corrupt or affect the administration, security, fairness, integrity or proper conduct of the promotion, the Promoters reserve the right to cancel, terminate, modify or suspend the promotion and/or to disqualify and/or ban any individual who (whether directly or indirectly) causes (or has caused or has attempted to cause) the problem. If an act, omission, event or circumstance occurs which is beyond the reasonable control of the Promoters and which prevents the Promoters from complying with these terms and conditions the Promoters will not be liable for any failure to perform or delay in performing their obligation.<br><br>
27. A failure by the Promoters to enforce any one of the terms and conditions in any instance(s) will not give rise to any claim or right of action by any entrant or prize winner, nor shall it be deemed to be a waiver of any of the Promoters’ rights in relation to the same.<br><br>
28.<br><br>
29. Winner Prize redemption will be as follows:<br><br>
The prize winners will be informed and contacted and requested to confirm and supply their address and/or proof of identity which they must submit to the Promoters within 5 (five) working days.<br><br>
30. The prize is non-transferable and cannot be exchanged for any alternatives in whole or in part. The decisions of the Promoters are final and no correspondence will be entered into. Winner is solely responsible for the payment of any applicable taxes associated with the Prize.<br><br>
31. The name of the winning entrants for the radio competition element will be posted on Yarona FM social media pages each week.<br><br>
32. Except as specifically set out herein and to the maximum extent permitted by law, all conditions, warranties and representations expressed or implied by law are hereby excluded. To the fullest extent permitted by law, the Promoters hereby exclude and shall not have any liability to any entrant or prize winner in connection with or arising out of this Competition howsoever caused, including for any costs, expenses, forfeited prizes, damages and other liabilities, provided that nothing herein shall operate so as to limit or exclude the Promoters’ liability for personal injury or death caused by its negligence or liability for fraudulent misrepresentation. For the avoidance of doubt, this paragraph shall also apply in respect of any prize provided by a third party provider.<br><br>
33. The winner’s name and image may be published on Yarona FM’s social media sites, and accordingly by participating in this promotional competition, all entrants are deemed to have read and understand the terms and conditions of the Facebook social media account and by entering this Competition, entrants further allow the Promoters permission to publish his/her name and photograph on various media platforms.<br><br>
34. The winners will be required to appear on any form of media and/or social media, for which no fee will be payable, and subject to the provision that the winners may at any time prior to such marketing appearance, decline the request by the Promoters to do so.<br><br>
35. The copyright, intellectual property rights, image rights and any other rights vesting in any Competition footage, posts, videos and photographs (whether depicting the winner and partner or not) shall remain the property of the Promoters, who reserve the right to use it in any way.<br><br>
36. Any entrant who transgresses any of these terms and conditions or who acts in any way contrary to the spirit of this promotional competition may be banned from entry into this, or into any other Promoters or PepsiCo promotional competitions, for a period deemed appropriate by Promoter management.<br><br>
37. Promoters reserve the right to cancel or amend or alter the Competition and its rules or prizes (not already awarded, to any other prize of comparable commercial value) at their own discretion at any time, if deemed necessary in their opinion and/or if circumstances arise outside of their control, without prior notice and no entrant shall have recourse for such cancellation or alteration. Any changes will be posted either within the competition information or these Terms shall become effective immediately after being altered or on such date as may be determined by Promoter. No entrant shall have any recourse against the Promoters as a result of the alterations of the Terms.<br><br>
38. Entries which are unclear, illegible, are submitted via an incorrect entry mechanism or contain errors or from Disqualified Persons, will be declared invalid. If the Promoter is unable to reach any entrant or complete the verification process after drawing his/her entry for whatsoever reason, such entrant will be disqualified and the draw of a replacement entry shall take place in the same manner.<br><br>
39. All ancillary costs, including but not limited to transport, meals, personal and incidental expenses, insurance, government taxes or other fees applicable, are the responsibility of the winner and/or entrants.<br><br>
40. All entrants and the winners, as the case may be, indemnify the Promoters, their advertising agencies, advisers, nominated agents, suppliers and Bottlers of beverages identified by the trademarks owned by or licensed to PepsiCo Inc, its affiliates and/or associated companies against any and all claims of any nature whatsoever arising out of and/or from their participation in any way howsoever in this Competition (including, as a result of any act or omission, whether as a result of negligence, misrepresentation, misconduct or otherwise on the part of the Promoters and/or use of the prizes).<br><br>
41. All entrants are advised that in compliance with the laws of the countries in which the Promoters operate, the Promoters are required to retain certain information (where applicable) of entrants and winners for a period of 3 (three) years (“Personal Information”). Such Personal Information to be retained by the Promoters includes (but is not limited to) the full names, identity numbers, contact details and winners acknowledgement of prize receipts. Accordingly the Promoters will require the winners to complete and submit an information disclosure agreement and indemnification to enable the<br><br>
42. Promoters to ensure compliance with these rules. Should any entrant and/or winner refuse or be unable to comply with this rule for any reason, such entrant and/or winner will be deemed to have rejected the Prizes and it shall revert back to the Promoters.<br><br>
43. All queries in connection with this Competition should be directed to Tshepiso Mahabo, tshepiso@alvigordon.com herein the agency responsible for managing the competition on Simba’s behalf, and Simba Customer Care Line – 086 11 000 97.<br><br>
44. A copy of the competition rules is available to the entrants and can be downloaded from www.simbasoundsflavour.co.bw
            </div>
        </div>
<?php
include_once("rowfooter.php");
?>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
$(function () {
    $('#carouselExampleIndicators').carousel();
});
    </script>
</body>

</html>