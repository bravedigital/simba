<?php
include_once("functions.php");

$token = $_SESSION['token'];
$secrettoken = $_SESSION['secrettoken'];
// echo $secrettoken;
// die;

if (!ctype_xdigit($token)) {
    echo 2;
    die;
}
if (!ctype_xdigit($secrettoken)) {
    echo 3;
    die;
}

if ($token != "" && isset($_POST['txtName']) && isset($_POST['txtCell'])) {
    $winningtoken = $_SESSION['winningtoken'];

    $name = $_POST['txtName'];
    $cell = $_POST['txtCell'];

    $name = mysqli_real_escape_string($conn, $name);
    $name = str_replace("%", "\%", $name);

    $cell = mysqli_real_escape_string($conn, $cell);
    $cell = str_replace("_", "\_", $cell);

    $winningtoken = mysqli_real_escape_string($conn, $winningtoken);
    $winningtoken = str_replace("_", "\_", $winningtoken);

    $entriessql = "UPDATE entries SET name = '" . $name . "', cell = '" . $cell . "', voucher = '" . $winningtoken . "' WHERE token = '" . $token . "'";
    // echo $entriessql;
    // die;
    mysqli_query($conn, $entriessql);

    $_SESSION['winnername'] = $name;

    $_SESSION['gotopage'] = "redeem.php";

    $host = $_SERVER['HTTP_HOST'];
    $url = "https://" . $host  . "/" . $_SESSION['gotopage'];
    header("Location: " . $url);
}
else {
    die;
}

?>