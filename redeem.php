<?php
include_once("functions.php");

if ($_SESSION['gotopage'] != "redeem.php") {
    $host = $_SERVER['HTTP_HOST'];
    $url = "https://" . $host  . "/" . $_SESSION['gotopage'];
    header("Location: " . $url);
}

$name = $_SESSION['winnername'];
$code = getRandomHex();
?>
<!DOCTYPE HTML>
<html>

<head>
    <title>Simba Sounds Of Flavour! Redeem</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/x-icon" href="img/favicon.png" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/styles.css?c=<?=time()?>">
<?php
include_once("analytics.php");
?>
</head>

<body>
    <div class="containermain container-fluid">
<?php
include_once("rowheader.php");
?>
        <div class="row">
            <div class="col-12 text-center">
                <img src="assets/winner.png">
            </div>
        </div>
        <div class="row my-4">
            <div class="col-12 text-center">
                <span class="spnDetailsText">Thanks for playing, <?=$name?> - Congratulations!<br><br>Please present this code to the cashier after your next grocery purchase to claim your prize!</span>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-12">
                <img src="assets/hr.svg" class="imgHR">
            </div>
        </div>
        <div class="row my-4">
            <div class="col-12">
                <div id="divCode" class="spnWinningCode"><?=$code?></div>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-12">
                <img src="assets/hr.svg" class="imgHR">
            </div>
        </div>
        <div class="row my-4">
            <div class="col-12">
                <div id="divCode">
                    <a class="btn btn-success btn-lg btnRedeem" id="btnDownload" download>DOWNLOAD</a>
                </div>
            </div>
        </div>
<?php
include_once("rowfooter.php");
?>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<script>
$(function () {
    getCode();

    $("#btnDownload").on("click", function() {
        // window.location.href = $("#divCode img").attr("src");
    });
});

function getCode() {
    $.ajax({
        url: 'getcode.php',
        type: 'post',
        success: function(data) {
            if (data == 1) {
                $('#divCode').html("No codes left ...");
            }
            else if (data == 2 || data == 3) {
                $('#divCode').html("Error 2/3");
            }
            else if (data == 4) {
                $('#divCode').html("Error 4");
            }
            else {
                $('#divCode').html("<img src='img/codes/" + data + "' />");
                $("#btnDownload").attr("href", "img/codes/" + data);
            }
        }
    });
}
</script>
</body>

</html>