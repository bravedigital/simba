<?php
include_once("functions.php");

if ($_SESSION['gotopage'] != "play.php") {
    $host = $_SERVER['HTTP_HOST'];
    $url = "https://" . $host  . "/" . $_SESSION['gotopage'];
    header("Location: " . $url);
}
else {
    $_SESSION['gotopage'] = "result.php";
}

$errormessage = "";

if (isset($_GET['token'])) {
    $token = $_GET['token'];
    if (ctype_xdigit($token)) {     // Test for hexadecimal
        $sql = "SELECT token, redeemed FROM sessions WHERE token = '" . $token . "'";
        $result = mysqli_query($conn, $sql);
        $resultcount = mysqli_num_rows($result);
        if ($resultcount === 1) {
            $row = mysqli_fetch_assoc($result);
            $redeemed = $row['redeemed'];
            if ($redeemed == 0) {  // Token not used yet
                $tokenupdatesql = "UPDATE sessions SET redeemed = 1 WHERE token = '" . $token . "'";
                $tokenupdateresult = mysqli_query($conn, $tokenupdatesql);
            }
            else {
                $errormessage = "Token has already been redeemed. Get more Simba chips to get a new token.";
            }
        }
        else {
            $errormessage = "Token does not exist";
        }
    }
    else {
        $errormessage = "Invalid token format";
    }
}

$cheatbutton = "";
if (isset($_GET['cheatmode'])) {
    $cheatbutton = '<br><br><button type="button" class="btn btn-success btn-lg" id="btnCheat">CHEAT</button>';
}
?>
<!DOCTYPE HTML>
<html>

<head>
    <title>Simba Sounds Of Flavour! Play</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/x-icon" href="img/favicon.png" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/styles.css?c=<?=time()?>">
<?php
include_once("analytics.php");
?>
</head>

<body>
    <div class="containermain">
<?php
include_once("rowheader.php");
?>
        <div class="row">
            <div class="col-md-12 text-center">
                <img src="assets/roars.svg" class="imgMainCampaingTitle">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center divSubline">
                <object data="assets/subline2.svg"></object>
            </div>
        </div>
        <div class="row my-4 text-center">
            <div class="col-sm-12 no-padding">
                <?php
                if ($errormessage == "") {
                ?>
                <div class="slotwrapper" id="reel">
                    <ul id="reel1" class="ulReel1">
                        <li><img src="assets/Simba_export_120g_render_tomato.png" data-src="assets/Simba_export_120g_render_tomato.png" /></li>
                        <li><img src="assets/Simba_export_120g_render_chutney.png" data-src="assets/Simba_export_120g_render_chutney.png" /></li>
                        <li><img src="assets/Simba_export_120g_render_mex.png" data-src="assets/Simba_export_120g_render_mex.png" /></li>
                        <li><img src="assets/Simba_export_120g_render_beef.png" data-src="assets/Simba_export_120g_render_beef.png" /></li>
                        <li><img src="assets/Creamy-cheddar-120g-Simba-Export.png" data-src="assets/Creamy-cheddar-120g-Simba-Export.png" /></li>
                        <!-- <li><img src="assets/simba-salt-and-vinegar_342.png" data-src="assets/simba-salt-and-vinegar_342.png" /></li> -->
                    </ul>
                    <ul id="reel2" class="ulReel2">
                        <li><img src="assets/Simba_export_120g_render_tomato.png" data-src="assets/Simba_export_120g_render_tomato.png" /></li>
                        <li><img src="assets/Simba_export_120g_render_chutney.png" data-src="assets/Simba_export_120g_render_chutney.png" /></li>
                        <li><img src="assets/Simba_export_120g_render_mex.png" data-src="assets/Simba_export_120g_render_mex.png" /></li>
                        <li><img src="assets/Simba_export_120g_render_beef.png" data-src="assets/Simba_export_120g_render_beef.png" /></li>
                        <li><img src="assets/Creamy-cheddar-120g-Simba-Export.png" data-src="assets/Creamy-cheddar-120g-Simba-Export.png" /></li>
                        <!-- <li><img src="assets/simba-salt-and-vinegar_342.png" data-src="assets/simba-salt-and-vinegar_342.png" /></li> -->
                    </ul>
                    <ul id="reel3" class="ulReel3"> 
                        <li><img src="assets/Simba_export_120g_render_tomato.png" data-src="assets/Simba_export_120g_render_tomato.png" /></li>
                        <li><img src="assets/Simba_export_120g_render_chutney.png" data-src="assets/Simba_export_120g_render_chutney.png" /></li>
                        <li><img src="assets/Simba_export_120g_render_mex.png" data-src="assets/Simba_export_120g_render_mex.png" /></li>
                        <li><img src="assets/Simba_export_120g_render_beef.png" data-src="assets/Simba_export_120g_render_beef.png" /></li>
                        <li><img src="assets/Creamy-cheddar-120g-Simba-Export.png" data-src="assets/Creamy-cheddar-120g-Simba-Export.png" /></li>
                        <!-- <li><img src="assets/simba-salt-and-vinegar_342.png" data-src="assets/simba-salt-and-vinegar_342.png" /></li> -->
                    </ul>
                </div>
                <div class="row my-4">
                    <div class="col-md-12 text-center">
                        <div id="divNumSpins"></div>
                        <div id="divButtons">
                            <button type="button" class="btn btn-success btn-lg" id="btnSpin">SPIN!</button>
                            <?php
                                echo $cheatbutton;
                            ?>
                        </div>
                        <div id="divDebug"></div>
                        <div id="divCode"></div>
                    </div>
                </div>
                <?php
                }
                else {
                ?>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div id="divError"><?=$errormessage?></div>
                    </div>
                </div>
                <?php
                }
                ?>
            </div>
        </div>
<?php
include_once("rowfooter.php");
?>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="js/slotmachine.min.js"></script>

    <script type="text/javascript">
$(function () {
    var spinspeed = 2000;
    var spinloops = 3;
    var numspins = 0;
    var spinsremaining = 3;
    var spins = [];
    var win = false;
    $("#divNumSpins").html("0" + spinsremaining + " LEFT");

    $('#btnSpin, #btnCheat').on("click", function () {
        if (numspins == 0) {
            for(let i=1;i<=3;i++) {
                var originalimg = $("#reel" + i + " li:nth-child(1) img").attr("data-src");
                $("#reel" + i + " li:nth-child(1) img").attr("src", originalimg);
            }
        }

        numspins++;
        spinsremaining--;
        endnum = 0;

        var btnid = this.id;
        if (btnid == "btnCheat") {
            endnum = [1, 1, 1];
        }

        $('#reel ul').playSpin({
            time: spinspeed,
            loops: spinloops,
            endNum: endnum,
            onFinish: function (num) {
                spins[numspins] = num;
                if (num[0] == num[1] && num[1] == num[2]) {
                // if (true) {
                    win = true;
                    $("#divNumSpins").hide();
                    $("#divButtons").hide();
                    $("#divDebug").append("<span class='spnWin'>YOU WIN!</span>");
                    getCode();
                    getResult();
                }
                else {
                    if (spinsremaining == 0) {
                        $("#divNumSpins").html(spinsremaining + " LEFT");
                        $("#divButtons").hide();
                        // getTryAgain();
                        getResult();
                    }
                    else {
                        $("#divNumSpins").html("0" + spinsremaining + " LEFT");
                    }
                }
            }
        });

        if (spinsremaining == 0) {
            $("#divNumSpins").html(spinsremaining + " LEFT");
            $("#divButtons").hide();
        }
        else {
            $("#divNumSpins").html("0" + spinsremaining + " LEFT");
            $("#divButtons").show();
        }
    });

    for(let i=1;i<=3;i++) {
        var rnd = Math.floor((Math.random() * 5) + 1);
        var rndimg = $("#reel" + i + " li:nth-child(" + rnd + ") img").attr("data-src");
        $("#reel" + i + " li:nth-child(1) img").attr("src", rndimg);
    }
});

function getResult() {
    setTimeout(function() {
        window.location.replace("result.php");
    }, 1000);
}

function getCode() {
    $.ajax({
        url: 'getimagecode.php',
        type: 'post',
        success: function(data) {
            if (data == 1) {
                $('#divCode').html("No codes left ...");
            }
            else if (data == 2 || data == 3) {
                $('#divCode').html("Error 2/3");
            }
            else if (data == 4) {
                $('#divCode').html("Error 4");
            }
            else {
                // $('#divCode').html("<img src='data:image/jpg;base64," + data + "' />");
                $('#divCode').html("One moment ...");
            }
        }
    });
}

function getTryAgain() {
    $.ajax({
        url: 'gettryagain.php',
        type: 'post',
        success: function(data) {
            if (data == 1) {
                // $('#divCode').html("Get some more Simba chips and try again later!");
                $('#divCode').html("Get some more Simba chips and try again later! <a href='https://simba.bravedigital.co.za/entry.php?simba'>Go!</a>");
            }
            else {
                $('#divCode').html("Error 5");
            }
        }
    });
}
</script>
</body>

</html>