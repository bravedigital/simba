<?php
include_once("functions.php");

$image = "img/SimbaBGsmall.jpg";

$token = $_SESSION['token'];
$secrettoken = $_SESSION['secrettoken'];
// echo $secrettoken;
// die;

if (!ctype_xdigit($token)) {
    echo 2;
    die;
}
if (!ctype_xdigit($secrettoken)) {
    echo 3;
    die;
}

$fontsize = 30;
$x_axis = 80;
$y_axis = 160;

if ($_SESSION['store'] == "Botswana Square Mart") {
    $codesql = "SELECT code FROM newcodes WHERE redeemed = 0 AND store = 'Botswana Square Mart' ORDER BY id ASC LIMIT 1;";
    $fontsize = 24;
    $x_axis = 40;
    $y_axis = 140;
}
elseif ($_SESSION['store'] == "Botswana Choppies") {
    $codesql = "SELECT code FROM newcodes WHERE redeemed = 0 AND store = 'Botswana Choppies' ORDER BY id ASC LIMIT 1;";
    $fontsize = 24;
    $x_axis = 40;
    $y_axis = 140;
}
elseif ($_SESSION['store'] == "Botswana Spar") {
    $codesql = "SELECT code FROM newcodes WHERE redeemed = 0 AND store = 'Botswana Spar' ORDER BY id ASC LIMIT 1;";
}
else {
    // $codesql = "SELECT code FROM codes WHERE shopid = 1 AND redeemed = 0 ORDER BY id ASC LIMIT 1;";
    $codesql = "SELECT code FROM codes WHERE redeemed = 0 ORDER BY id ASC LIMIT 1;";
}
echo $codesql;
$result = mysqli_query($conn, $codesql);
$resultcount = mysqli_num_rows($result);
if ($resultcount === 1) {
    $row = mysqli_fetch_assoc($result);
    $code = $row['code'];

    if ($code) {
        if ($_SESSION['store'] == "Botswana Square Mart" || $_SESSION['store'] == "Botswana Choppies" || $_SESSION['store'] == "Botswana Spar") {
            $codeupdatesql = "UPDATE newcodes SET redeemed = 1, datetaken = NOW() WHERE code = '" . $code . "'";
        }
        else {
            $codeupdatesql = "UPDATE codes SET redeemed = 1, datetaken = NOW() WHERE code = '" . $code . "'";
        }
        $codeupdateresult = mysqli_query($conn, $codeupdatesql);
        echo $codeupdatesql . "<br>";

        $sessionupdatesql = "UPDATE sessions SET won = 1 WHERE token = '" . $token . "'";
        $sessionupdateresult = mysqli_query($conn, $sessionupdatesql);
        echo $sessionupdatesql;
        // die;
    }
}
else {
    echo 1;     // No codes left
    die;
}
// $code = getRandomHex(3);

$codefordisplay = str_replace(" & ", "\n", $code);
$codeforimagename = str_replace(" & ", "", $code);

$white = "#FFFFFF";
$blue = "blue";
$red = "red";
$simbayellow = "#F7D40C";
$grey = "#C5C5C5";

if (class_exists('Imagick')) {
    $imagick = new Imagick();
    $imagick->newImage(300, 300, new ImagickPixel($white));
    $imagick->setImageFormat('jpg');

    $draw = new ImagickDraw();
    $draw->setStrokeColor($grey);
    $draw->setStrokeWidth(1);
    $draw->setFillColor($grey);
    $draw->setFontSize($fontsize);
    $draw->setFont("fonts/KGHAPPYSolid.ttf");

    $imagick->setImageBackgroundColor($white);
    $imagick->annotateimage($draw, $x_axis, $y_axis, 0, $codefordisplay);
    $imagick->writeImage("img/codes/" . $codeforimagename . '.png');

    $_SESSION['winningtoken'] = $token;
    $_SESSION['winningcode'] = $codeforimagename . '.png';
    $_SESSION['won'] = true;
    echo $_SESSION['winningcode'];
    // header('Content-type: image/png');
    echo $codeforimagename . '.png';

    // $imagick = new Imagick(realpath($image));

    // $draw = new ImagickDraw();
    // $draw->setStrokeColor($simbayellow);
    // // $draw->setFillColor($simbayellow);

    // $draw->setStrokeWidth(2);
    // $draw->setFontSize(36);

    // $draw->setFont("fonts/KGHAPPYSolid.ttf");
    // $imagick->annotateimage($draw, 210, 290, 0, $code);
    // // $draw->setFont("fonts/Hackney-Vector.ttf");
    // // $imagick->annotateimage($draw, 220, 480, 0, $code);

    // $_SESSION['won'] = true;

    // header('Content-type: image/png');
    // echo base64_encode($imagick);
}
else {
    echo 4;
    die;
}
?>