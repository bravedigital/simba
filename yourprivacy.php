<!DOCTYPE HTML>
<html>

<head>
    <title>Simba Sounds Of Flavour! Privacy</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/x-icon" href="img/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/styles.css?c=<?=time()?>">
<?php
include_once("analytics.php");
?>
</head>

<body>
    <div class="containermain">
<?php
include_once("rowheader.php");
?>
        <div class="row">
            <div class="col-md-12 text-center">
                <object data="assets/your_privacy2.svg"></object>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-10 divYourPrivacy">
External Privacy Notice<br><br>
<h6>1. PURPOSE OF THIS PRIVACY NOTICE</h6>
1.1.    This Privacy Notice (“Notice”) describes how and when Simba (Pty) Ltd (Registration No. 1995/003667/07), located at 6th Floor, 144 Oxford Road, Rosebank, Johannesburg, 2196 and Pioneer Food Group (Pty) Ltd (Registration No. 1996/017676/07), located at Glacier Place, 1 Sportica Crescent, Tygervalley, Western Cape, 7550, and their Affiliates (collectively “ PepsiCo SSA”, " We", "Us ", or "Our ")" collects, uses, and shares personally identifiable information (“Personal Information ”).<br><br>
1.2.    Personal Information includes Special Personal Information and Personal Information of both natural persons and legal entities.<br><br>
1.3.    This Privacy Notice applies to all external parties with whom We engage as part of Our business operations (“Data Subjects”/”You”/”Your”) and where as part of such engagement, We may collect, use, and share Your Personal Information.<br><br>
1.4.    As part of a global company, PepsiCo SSA, in the operation of its business uses several centralised functions and systems across the PepsiCo Group. Your Personal Information may be processed through these centralised functions and systems. Such Processing will be in accordance with the purpose and in the manner as set out in this Privacy Notice.<br><br>
1.5.    If you have questions about this Privacy Notice, please send an email to ssaprivacyoffice@pepsico.com<br><br>
<h6>2. WHAT PERSONAL INFORMATION WE COLLECT</h6>
PepsiCo SSA, in the conduct of its business operations, mostly engages and contracts with Juristic Entities. In dealing with such entities, We also engage with individuals within the Entities and collect and use the Personal Information of such persons. We will Process the Personal Information of such individuals as stated in this Privacy Notice.<br><br>
2.1.   Personal Information We collect.<br><br>
2.1.1.Contact Information such as Company name, contact persons, e-mail addresses, physical address, phone and mobile numbers;<br><br>
2.1.2.Account and payment information such as authorised account users, VAT and Income tax reference numbers, bank details, invoices and records, SWIFT and IBAN details;<br><br>
2.1.3.Verification Information such as username, password, password reminder questions and password answers;<br><br>
2.1.4.Identifiers such as Company registration numbers, identity numbers, Supplier/Vendor number, B-BBEE status, vehicle registration numbers, driver’s license, physical addresses;<br><br>
2.1.5.Contract information including product or services information, commercial terms of the contract, contract performance and non-performance;<br><br>
2.1.6.Tender request information including client references, proof of insurance and tax clearance certificate of good standing, account details;<br><br>
2.1.7.Website, Mobile Applications and Social Media information: when you access any PepsiCo SSA website (or special purpose portals) including any brand websites, We collect your IP address and other technical information about your computer and website usage;<br><br>
2.1.8.Financial information such as financial statements, credit and trade references, shareholders, trustees and sureties;<br><br>
2.1.9.Background and Credit information including credit status and depending on the nature of the services/contract, criminal record and fraud checks;<br><br>
2.1.10.    Principles Information: where You apply for credit, the name, ID numbers, and residential addresses of Your Partners, Shareholders and Directors and details of property/is owned by Principals (Sole Owner / Partners / Members / Directors);<br><br>
2.1.11.    CCTV: we may process videos of You obtained via our CCTV surveillance systems;<br><br>
2.1.12.    Recruitment information: CV/resume, SA ID number, passport number; training records, education and work history, address, contact details, certifications/qualifications;<br><br>
2.1.13.    Information relating to Individual Consumers of our Products: such as name, phone number, email address, physical address, mobile phone number, preferred language, details of complaints, bank account details for payment of any agreed settlement amount, medical information were the basis if the claim is health related.<br><br>
<h6>3. If You Do Not Provide Us with The Personal Information</h6>
If You do not provide Us with the Personal Information, by way of illustration only:<br><br>
3.1.1. We may not be able to onboard You as a Vendor/Customer;<br><br>
3.1.2. We may not be able to conclude a contract to do business with You;<br><br>
3.1.3. We may not be able to respond to enquiries or investigate a complaint received from You.<br><br>
<h6>4. HOW WE COLLECT PERSONAL INFORMATION:</h6>
4.1. We collect information directly from You when:<br><br>
4.1.1. We engage directly with You as part of any onboarding process;<br><br>
4.1.2. We negotiate and conclude an agreement with You;<br><br>
4.1.3. You engage with Us, such as through emails, complaints or queries via Our call centres;<br><br>
4.1.4. You enter any of Our Sites, Offices, Facilities and work areas (for example via CCTV);<br><br>
4.1.5. You interact with Our support, sales and account management teams;<br><br>
4.1.6. You render any services to Us as a third party service provider or sell or purchase Products from Us;<br><br>
4.1.7. when You reply to a Request for Quotation or Request for Tenders;<br><br>
4.1.8. You apply for credit.<br><br>
4.2. We collect Personal Information indirectly about You from:<br><br>
4.2.1. other companies in the PepsiCo Group of companies;<br><br>
4.2.2. Websites, by phone, email, or through Your participation in a survey conducted through third party websites (including Our branded sites which may be managed by third parties) or through social media platforms such as Facebook, Snapchat, Instagram, LinkedIn and TikTok;<br><br>
4.2.3. public sources and registers (such as company registers, online search engines, (title) deeds registries, public posts on social media);<br><br>
4.2.4. third parties that We engage for the purposes of conducting its business (list providers, credit bureaux, regulators and government departments and third party service providers);<br><br>
4.2.5. employment agencies, background check providers, former employers, credit reference agencies or other background check agencies.<br><br>
<h6>5. WHAT WE USE YOUR PERSONAL INFORMATION FOR</h6>
We Use Your Personal information –<br><br>
5.1. to operate Our business;<br><br>
5.2. to implement and manage relationships with You including onboarding as a Vendor or Customer, processing and fulfilling orders, sending invoices, processing payments, accounting, auditing, billing, collection and returns;<br><br>
5.3. to provide customer services (for example, product recalls, tracking, and responding to product quality concerns);<br><br>
5.4. to comply with any legislation or regulation which requires Us to collect the information;<br><br>
5.5. to comply with demands or requests made by Regulators, Governmental Authorities and Law Enforcement Authorities;<br><br>
5.6. to monitor and analyse trends, usage and activities in connection with Our products;<br><br>
5.7. to improve Our products;<br><br>
5.8. to ensure We have Your up-to-date contact information;<br><br>
5.9. to conduct internal audits and investigations;<br><br>
5.10. to provide insurance and to process insurance claims;<br><br>
5.11. to verify information provided by You.<br><br>
<h6>6. SPECIAL PERSONAL INFORMATION</h6>
Where we need to Process your Special Personal Information, we will do so in the ordinary course of our business, for a legitimate purpose, and in accordance with applicable laws.<br><br>
<h6>7. WHO WE SHARE THE PERSONAL INFORMATION WITH?</h6>
7.1. We may share Your Personal Information with –<br><br>
7.1.1. third-party service providers who provide services to Us, including but not limiting to supporting, operating, securing and hosting Our information technology systems, providers of customer experience services, payment processing, order fulfilment, product management, logistics and returns, debt collection, document and information storage;<br><br>
7.1.2. Our professional advisors;<br><br>
7.1.3. law enforcement, government officials, or other third parties as may be necessary or appropriate in connection with an investigation of fraud, intellectual property infringements, or other activity that is illegal or may expose Us to legal liability;<br><br>
7.1.4. third parties (such as a potential purchaser and its professional advisors) in the event of any reorganisation, merger, divestiture, acquisition, consolidation, restructure, sale, joint venture, or other disposition of any or all of our assets.<br><br>
<h6>8. PERSONAL INFORMATION OF CHILDREN</h6>
We do not intentionally collect Personal Information from children under the age of 18 years.<br><br>
<h6>9. TRANSFER OF PERSONAL INFORMATION TO OTHER COUNTRIES?</h6>
9.1. We are a global organization and may transfer certain Personal Information across geographical borders to –<br><br>
9.1.1. PepsiCo Group companies;<br><br>
9.1.2. service providers for the purposes set out in 8 above;<br><br>
9.1.3. verify and undertake risk assessments of new Vendors/Customers.<br><br>
9.2. Where We transfer Your Personal Information outside of South Africa, We ensure that We do so in accordance with the requirements for lawful transfer outside of South Africa as set out in the Protection of Personal Information Act No.4 of 2013 “POPIA”).<br><br>
9.3. As part of a global company, in the operation of Our business We use several centralised functions and systems across the PepsiCo Group. Your Personal Information may be processed through these centralised functions and systems. Such Processing will be in accordance with the purpose and in the manner as set out in this Privacy Notice.<br><br>
9.4. Examples of PepsiCo Regions countries (sectors) We may transfer personal information includes North America, Latin America, Europe, AMESA Sector (Africa, Middle East and South Asia) and APAC Sector (Asia Pacific, Australia/New Zealand and China).<br><br>
9.5. You consent to the transfer of Your Personal Information outside of South Africa across geographical borders for the purposes set out in this section 9.<br><br>
<h6>10. HOW LONG DO WE KEEP YOUR PERSONAL INFORMATION?</h6>
10.1. We will retain your Personal Information for as long as necessary to achieve the purpose for which it was collected or subsequently processed.<br><br>
10.2. Personal Information may be held for longer periods where - 10.2.1. retention of the record is required or authorised by law;<br><br>
10.2.2. We reasonably require the record for lawful purposes related to Our functions or activities;<br><br>
10.2.3. retention of the record is required by a contract between You and Us; or<br><br>
10.2.4. You have consented to the retention of the record.<br><br>
<h6>11. HOW WE KEEP YOUR PERSONAL INFORMATION SECURE</h6>
11.1. We secure the integrity and confidentiality of Personal Information in Our possession or under Our control by taking appropriate, reasonable technical and organisational measures to prevent loss of, damage to or unauthorised destruction of Personal Information; and unlawful access to or processing of Personal Information.<br><br>
11.2. In order to ensure such security, We have in place policies, controls and related processes, which are reviewed and updated on a regular basis.<br><br>
11.3. Our policies, controls and procedures cover for example 11.3.1. physical, technical and network security;<br><br>
11.3.2. access controls and monitoring access;<br><br>
11.3.3. secure storage, destruction and encryption of records of Personal Information;<br><br>
11.3.4. Personal Information security incident reporting and remediation;<br><br>
11.3.5. by way of written agreements, imposition of security and confidentiality obligations on Operators and reserving the right to audit their systems to ensure compliance with such obligations .<br><br>
<h6>12. COOKIES</h6>
12.1. We may place small text files called ‘cookies’ on Your device when You visit Our website. Cookies do not contain Personal Information, but they do contain a personal identifier allowing Us to associate Your Personal Information with a certain device.<br><br>
12.2. We use the following types of cookies on Our website:<br><br>
12.2.1. Essential cookies which are crucial to a user’s experience of a website, enabling core features functions on Our website;<br><br>
12.2.2. Performance cookies which track how You use a website during Your visit. This information is typically anonymous and aggregated and helps Us understand visitor usage patterns, identify and diagnose problems users may encounter, and make better decisions in improving the overall website experience;<br><br>
12.2.3. Functionality cookies which are used to collect information about Your device and any settings you may configure on the Website you’re visiting (like language and time zone settings). With this information, websites can provide a customized, enhanced, or optimized content and services.<br><br>
12.3. Your web browser can be set to allow You to control whether you will accept or reject cookies, or to notify You each time a cookie is sent to Your browser. If Your browser is set to reject cookies, websites that are cookie-enabled will not recognize You when you return to the website, and some website functionality may be lost.<br><br>
12.4. You consent to our use of cookies in accordance with this policy<br><br>
<h6>13. YOUR RIGHTS</h6>
13.1. You have the right to –<br><br>
13.1.1. request a record of Your Personal Information. We may charge You a fee to provide such a record, but We will tell You how much this is beforehand. In certain circumstances, We cannot tell you what Personal Information We have about You. For example, to protect (a) the privacy of others; (b) confidential information of third parties; (c) the safety of others;<br><br>
13.1.2. request correction of Your Personal Information if it is inaccurate, irrelevant, excessive, misleading or obtained unlawfully;<br><br>
13.1.3. request deletion of Your Personal Information where there is no lawful basis to retain it;<br><br>
13.1.4. withdraw consent previously given in respect of the processing of Your Personal Information. Withdrawal of consent may limit Our or a third party’s ability to carry out certain functions in respect of Our commercial relationship.<br><br>
13.2. We will comply with Your request unless We have a reasonable explanation why We cannot comply.<br><br>
13.3. If We cannot agree whether to correct or delete the Personal Information and if You ask Us to, We will indicate on the Personal Information register that a correction was requested but was not made.<br><br>
13.4. If changes to the Personal Information impact any earlier decisions that impact You, if reasonably practicable, We will inform all persons to whom the Personal Information has been disclosed of those steps.  <br><br>
13.5. We will notify You of the action taken by Us because of Your request.<br><br>
13.6. All requests can be made via email at ssaprivacyoffice@pepsico.com We will always require that You verify You are in fact the owner of such Personal Information.<br><br>
<h6>14. RIGHT TO WITHDRAW CONSENT</h6>
14.1. In the limited circumstances where processing of Your Personal Information is based on Your Consent, You have the right to withdraw such consent at any time. To withdraw Your consent, please send an email to ssaprivacyoffice@pepsico.com<br><br>
14.2. Once We are notified that You have withdrawn Your consent, We will no longer process Your Personal Information for the purpose You originally agreed to, unless We have another lawful basis for doing so.<br><br>
<h6>15. HOW TO LODGE A COMPLAINT WITH THE INFORMATION REGULATOR</h6>
You may lodge a complaint in respect of this Privacy Notice or any of Our Personal Information Processing activities with the Information Regulator, whose contact details are available on its website at www.justice.gov.za/inforeg or www.justice.gov.za<br><br>
<h6>16. REVISIONS TO THE NOTICE</h6>
We may update this Privacy Notice from time to time. Changes to this Privacy Notice will not necessarily be preceded by a notice posted on the website, and we encourage you to check the Privacy Notice periodically for changes.<br><br>
This Privacy Notice was last updated on 09 JUNE 2021
            </div>
        </div>
<?php
include_once("rowfooter.php");
?>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
$(function () {
    $('#carouselExampleIndicators').carousel();
});
    </script>
</body>

</html>